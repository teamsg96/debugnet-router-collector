/*
 * WirelessSignal.cpp
 *
 *  Created on: 20 Feb 2019
 *      Author: sgasper
 */

#include "WirelessSignal.h"

WirelessSignal::WirelessSignal() {
	logger.setFile(WIRELESS_SIGNAL_LOG_FILE);
	// All wireless NICs on the router consist of a 2.4Ghz WiFi NIC and a 5Ghz
	// WiFi NIC.
	allNics.push_back(getConfig("nic_name_wifi_2_4_ghz").getValue<string>());
	allNics.push_back(getConfig("nic_name_wifi_5_ghz").getValue<string>());

	waitPeriod = getConfig("signal_wait_between_measures").getValue<int>();
}

/*
 * Execute 'iw' tool to fetch a dump of information about each device connected
 * to the router over WiFi.
 */
string WirelessSignal::fetchWirelessStationDump() {
	string dumpOut;

	// Execute the command for each NIC on the router
	for (string nic : allNics){
		string dumpCmd = "iw " + nic + " station dump";
		CommandResult result = executeCmdOnRouter(dumpCmd);

		if (result.exitCode != 0){
			throw CMDExecutionOnRouterException(dumpCmd, result.out);
		}

		// Append output for NIC
		dumpOut += result.out;
	}

	return dumpOut;
}

/*
 * Parse the output produced by 'iw' and return a metric record for each device
 */
vector<MetricRecord> WirelessSignal::parseDump(string rawDumpOut, time_t execTime){
	vector<Device> onlineDevices = getAllOnlineWirelessDevices();
	vector<MetricRecord> signalRecords;

	// The output consists of a textual dump for each WiFi connected device.
	// Each dump starts with a MAC address of the device and following that
	// is a key value pair pattern for various attributes of the device. The
	// loop below iterates over each MAC address occurrence and each signal value
	// key value occurrence in parallel. Each signal value occurrence is in line
	// with each MAC address occurrence.
	regex macLinePattern("Station ([a-fA-F0-9:]{17}|[a-fA-F0-9]{12})");
	regex signalLinePattern("signal:\\s+\\D(\\d+)");
	sregex_iterator macNext(rawDumpOut.begin(), rawDumpOut.end(), macLinePattern);
	sregex_iterator macEnd;
	sregex_iterator signalNext(rawDumpOut.begin(), rawDumpOut.end(), signalLinePattern);
	sregex_iterator signalEnd;

	while (macNext != macEnd && signalNext != signalEnd){
		// Get the next MAC match and signal value match
		smatch macMatch = *macNext;
		smatch signalValMatch = *signalNext;

		// Convert the first (and only) group found in the match to an string and
		// ensure all MAC characters are upper case.
		string mac = macMatch.str(1);
		for (auto& c: mac) c = toupper(c);

		// Convert the first (and only) group found in the match to an int. This
		// is the signal value for the MAC address match.
		int signalVal = stoi(signalValMatch.str(1));

		// Prepare for going to next matches
		macNext++;
		signalNext++;

		// The 'iw' may pick up new devices that the discovery services has not
		// yet had chance to discover. If 'iw' produces a MAC not known then it
		// and its value is ignored. Otherwise the DB will throw and error
		// stating that the MAC of the device is not in the table of devices (as
		// its a foreign key).
		bool deviceWithMacKnown = false;
		for (unsigned int i=0; i < onlineDevices.size(); i++){
			if (onlineDevices[i].getMacAddress() == mac){
				deviceWithMacKnown = true;
				// To make the search shorter next time the device is removed
				// from the list
				onlineDevices.erase(onlineDevices.begin() + i);
				break;
			}
		}

		if (deviceWithMacKnown == false){
			// Dont record signal value, as the device is not known to the DB.
			continue;
		}

		// Record device and its signal value
		MetricRecord metRecord(execTime);
		metRecord.addParam("signal", signalVal);
		metRecord.addParam("device", mac);
		signalRecords.push_back(metRecord);
	}

	// Ensure their is a signal value strength for every MAC address
	if (macNext != macEnd || signalNext != signalEnd){
		throw DebugnetException("number of Mac's found != number of signal values found");
	}

	return signalRecords;
}

string WirelessSignal::getCollectorName() {
	return "WirelessSignal";
}

string WirelessSignal::getLogFilePath() {
	return WIRELESS_SIGNAL_LOG_FILE;
}

/*
 * Measure wireless signal to connected devices continuously
 */
void WirelessSignal::measure() {
	while (true){
		time_t execTime = time(0);

		string rawDumpOut = fetchWirelessStationDump();

		vector<MetricRecord> signalRecords = parseDump(rawDumpOut, execTime);

		// Log results
		string logMsg = "current wireless signal strengths...";
		for (MetricRecord record : signalRecords){
			logMsg += "\n\tMAC: " + record.getParam<string>("device") + \
						", signal: " + record.getParam<string>("signal");
		}
		logger.log(logMsg);

		// Store metrics to DB
		saveMetrics(WIRELESS_SIGNAL_TABLE, signalRecords);

		logger.log("waiting till next measure...");

		// Wait a set amount of time before measuring again
		this_thread::sleep_for(chrono::seconds(waitPeriod));
	}
}
