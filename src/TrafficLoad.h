/*
 * TrafficLoad.h
 *
 *  Created on: 17 Feb 2019
 *      Author: sgasper
 */

#ifndef TRAFFICLOAD_H_
#define TRAFFICLOAD_H_

#include <iomanip>
#include <iostream>
#include <math.h>
#include <thread>
#include <vector>

#include "CollectorABC.h"
#include "debugnet.h"

using namespace std;

const int IPTABLE_LOCK_WAIT = 10;
const string CHAIN_NAME = "DEBUGNET_CTR";
const string IPTABLE_CMD_PREF = "iptables -w " + to_string(IPTABLE_LOCK_WAIT) + " ";
const string TRAFFIC_LOG_FILE = "/var/log/debugnet-collector-traffic.log";

class TrafficLoad: public CollectorABC {
private:
	int byteAccPeriod;
	Logger logger;
	map<string, MetricRecord> IpToDevMetricEntry;

	void calculateBps(string iptableOut, time_t execTime,
			vector<MetricRecord>& deviceBps, MetricRecord& netTotalBps);
	string collectByteCounts();
	void ensureIpTableChainsExist();
	string humanReadableBpsResults(MetricRecord record);
	void refreshIpTableRules(time_t execTime);
public:
	TrafficLoad();
	string getCollectorName();
	string getLogFilePath();
	void measure();
};

#endif /* TRAFFICLOAD_H_ */
