/*
 * debugnet-collector.cpp
 *
 *  Created on: 16 Jan 2019
 *      Author: sgasper
 */

#include "debugnet-collector.h"

/*
 * Executes the 'measure' method of a collector inside a thread
 */
void runCollector(CollectorABC* collector){
	collector->measure();
}

int main() {
	vector<thread> activeThreads;
	vector<CollectorABC*> metricCollectors;

	cout << "Starting Debugnet collector..." << endl;

	// Initialise all collector modules here
	metricCollectors.push_back(new BandwidthISP());
	metricCollectors.push_back(new LatencyDevices());
	metricCollectors.push_back(new TrafficLoad());
	metricCollectors.push_back(new WirelessSignal());

	cout << "Loading collector modules...\n" << endl;
	for (auto & metricCollector : metricCollectors){
		cout << "collector module started: " + metricCollector->getCollectorName() + \
				", log:" + metricCollector->getLogFilePath() << endl;
		activeThreads.push_back(thread(runCollector, metricCollector));
	}

	// The collector should continuously run until some external process stops.
	// So the threads should never join unless an issue occurs.
	for (thread& th: activeThreads){
		th.join();
	}

	return 0;
}
