/*
 * TrafficLoad.cpp
 *
 *  Created on: 17 Feb 2019
 *      Author: sgasper
 */

#include "TrafficLoad.h"

TrafficLoad::TrafficLoad(){
	byteAccPeriod = getConfig("byte_acc_period").getValue<int>();
	logger.setFile(TRAFFIC_LOG_FILE);
}

/*
 * Calculate bytes per sounds from the byte counts of iptables
 */
void TrafficLoad::calculateBps(string iptableOut, time_t execTime,
		vector<MetricRecord>& deviceBps, MetricRecord& netTotalBps){
	// A record is kept of the total in bound and out traffic of the network.
	int netTotalBpsIn = 0;
	int netTotalBpsOut = 0;

	istringstream outStream(iptableOut);

	regex pattern("\\s+");
	string rawLine;

	// Skip first two lines as these contain unimportant information
	getline(outStream, rawLine);
	getline(outStream, rawLine);

	// Iterate over each byte
	while(getline(outStream, rawLine)){
		// Break the line up into 'columns'
		vector<string> lineParams{sregex_token_iterator(rawLine.begin(),
														rawLine.end(),
														pattern,
														-1), {}};

		// Calculate bytes per second. To do this the byte count is divided
		// by the period of time the counter was allowed to run for.
		int bps = round(stod(lineParams[2]) / static_cast<double>(byteAccPeriod));

		// Get the source and destination IP address of the byte count. One
		// address will be of a networked device and the other will
		// be 0.0.0.0/24, which means anything.
		string src = lineParams[7];
		string dst = lineParams[8];

		// For recording the actual device IP address in line (from src and dst)
		string ruleIp;

		// Determine which of the source and destination address is the address
		// of a device.
		if (IpToDevMetricEntry.find(src) != IpToDevMetricEntry.end()){
			// Record B/s for out bound traffic of the device
			IpToDevMetricEntry[src].addParam("bps_out", bps);
			netTotalBpsOut += bps;
			ruleIp = src;
		}
		else if (IpToDevMetricEntry.find(dst) != IpToDevMetricEntry.end()){
			// Record B/s for in bound traffic of the device
			IpToDevMetricEntry[dst].addParam("bps_in", bps);
			netTotalBpsIn += bps;
			ruleIp = dst;
		}
		else{
			throw DebugnetException("cannot identify IP for byte count. src=" + src + ", dst=" + dst);
		}

		// If B/s for in and out has been added for the device, then the total
		// B/s can be calculated and added for the device.
		if (IpToDevMetricEntry[ruleIp].hasParam("bps_in") == true &&
				IpToDevMetricEntry[ruleIp].hasParam("bps_out") == true){
			int totalBps = IpToDevMetricEntry[ruleIp].getParam<int>("bps_in") + \
					IpToDevMetricEntry[ruleIp].getParam<int>("bps_out");

			IpToDevMetricEntry[ruleIp].addParam("bps_total", totalBps);

			// All required traffic measurements collected for device. So it
			// it can be added to the vector.
			deviceBps.push_back(IpToDevMetricEntry[ruleIp]);
		}
	}

	// Formalise the total traffic rate of network as a MetricRecord.
	netTotalBps.addParam("bps_in", netTotalBpsIn);
	netTotalBps.addParam("bps_out", netTotalBpsOut);
	netTotalBps.addParam("bps_total", netTotalBpsIn + netTotalBpsOut);
}

/*
 * Collect byte counts from iptables. Returns output from iptables.
 */
string TrafficLoad::collectByteCounts() {
	// Args:
	//		-v -> show byte and packet count
	//		-x -> stick to bytes
	//		-n -> dont resolve hostnames
	string collectBytesCmd = IPTABLE_CMD_PREF + "-L " + CHAIN_NAME + " -v -x -n";

	CommandResult bytesResult = executeCmdOnRouter(collectBytesCmd);

	if (bytesResult.exitCode != 0){
		throw CMDExecutionOnRouterException(collectBytesCmd, bytesResult.out);
	}

	return bytesResult.out;
}

/*
 * Ensure a chain exists for DebugNet. If it doesn't exist then it is created.
 */
void TrafficLoad::ensureIpTableChainsExist() {
	logger.log("checking chain exists for DebugNet in iptables...");

	string queryChainExistanceCmd = IPTABLE_CMD_PREF + "-n --list " + CHAIN_NAME;
	CommandResult queryResult = executeCmdOnRouter(queryChainExistanceCmd);

	if (queryResult.exitCode == 0){
		// Chain already exists
		logger.log("iptable chain exists for DebugNet.");
		return;
	}
	else if (queryResult.exitCode == 1){
		// A chain for debugnet does not exist. So it is created here
		logger.log("chain does not exist for DebugNet in iptables. creating chain ...");

		// Generate the chain
		string createChainCmd = IPTABLE_CMD_PREF + "-N " + CHAIN_NAME;
		CommandResult chainCreationResult = executeCmdOnRouter(createChainCmd);

		if (chainCreationResult.exitCode != 0){
			throw CMDExecutionOnRouterException(createChainCmd,
					chainCreationResult.out);
		}

		// Add redirect for all traffic going throw the router to use the newly
		// created chain
		string redirectCmd = IPTABLE_CMD_PREF + "-I FORWARD -j " + CHAIN_NAME;
		CommandResult redirectResult = executeCmdOnRouter(redirectCmd);

		if (redirectResult.exitCode != 0){
			throw CMDExecutionOnRouterException(redirectCmd,
					redirectResult.out);
		}

	}
	else{
		throw CMDExecutionOnRouterException(queryChainExistanceCmd,
				queryResult.out);
	}
}

/*
 * Return the name of this collector
 */
string TrafficLoad::getCollectorName(){
	return "TrafficLoad";
}

/*
 * Return the file path this collector logs to
 */
string TrafficLoad::getLogFilePath(){
	return TRAFFIC_LOG_FILE;
}

/*
 * Structure B/s results in a human friendly way
 */
string TrafficLoad::humanReadableBpsResults(MetricRecord record){
	vector<string> bpsKeys = {"bps_in", "bps_out", "bps_total"};

	stringstream humanBps;

	// Format each B/s key and value accordingly
	for (unsigned int i = 0; i < bpsKeys.size(); i++){
		int bps = record.getParam<int>(bpsKeys[i]);

		// Separate if not the first key
		if (i != 0){
			humanBps << ", ";
		}

		humanBps << bpsKeys[i] << ": ";

		// Convert B/s to KB/s or MB/s if the value is large enough
		if (bps < 1000){
			humanBps << fixed << setprecision(2) << bps << " B/s";
		}
		else if (bps >= 1000 and bps < 1000000){
			humanBps << fixed << setprecision(2) << bytesToKilobytes(bps) << " KB/s";
		}
		else{
			humanBps << fixed << setprecision(2) << bytesToMegabytes(bps) << " MB/s";
		}
	}

	return humanBps.str();
}

/*
 * Clear any existing device rules in the iptables and create new ones for
 * current online devices.
 */
void TrafficLoad::refreshIpTableRules(time_t execTime) {
	// Clear all existing rules in the chain.
	string clearRulesCmd = IPTABLE_CMD_PREF + "-F " + CHAIN_NAME;
	CommandResult clearResult = executeCmdOnRouter(clearRulesCmd);
	if (clearResult.exitCode != 0){
		throw CMDExecutionOnRouterException(clearRulesCmd, clearResult.out);
	}

	// Create a rule in iptables for each online device
	for (Device device : getAllOnlineDevices()){
		MetricRecord devRec(execTime);

		devRec.addParam("device", device.getMacAddress());

		// Map the metric entries for each device against a devices IP. This
		// makes for easier access later on.
		IpToDevMetricEntry[device.getIpAddress()] = devRec;

		vector<string> ends = {"src", "dst"};

		// Create a rule for packets going to the device and another rule
		// for packets going from the device.
		for (string end : ends){
			string ruleCreateCmd = IPTABLE_CMD_PREF + "-A " + CHAIN_NAME + \
					" --" + end + " " + device.getIpAddress();

			CommandResult result = executeCmdOnRouter(ruleCreateCmd);

			if (result.exitCode != 0){
				throw CMDExecutionOnRouterException(ruleCreateCmd, result.out);
			}
		}
	}

	// Zero chain so that earlier rules have not had more time to let bytes
	// accumulate.
	string zeroCountersCmd = IPTABLE_CMD_PREF + "-Z " + CHAIN_NAME;
	CommandResult zeroResult = executeCmdOnRouter(zeroCountersCmd);
	if (zeroResult.exitCode != 0){
		CMDExecutionOnRouterException(zeroResult.cmd, zeroResult.out);
	}
}

/*
 * Measure the traffic load of the network
 */
void TrafficLoad::measure() {
	ensureIpTableChainsExist();

	while (true){
		IpToDevMetricEntry.clear();

		time_t execTime = time(0);

		refreshIpTableRules(execTime);

		// Give time for byte counts to accumulate
		this_thread::sleep_for(chrono::seconds(byteAccPeriod));

		// Use iptables to get the raw byte counts
		string iptableOut = collectByteCounts();

		// Parse the iptable output to get B/s of every online device and B/s
		// for the entire network.
		vector<MetricRecord> deviceBps;
		MetricRecord netTotalBps(execTime);
		calculateBps(iptableOut, execTime, deviceBps, netTotalBps);

		// Log device results
		string logMsg = "current traffic load...\n";
		logMsg += "\tNetwork devices:";
		for (MetricRecord deviceRec : deviceBps){
			logMsg += "\n\t\tMAC: " + deviceRec.getParam<string>("device") + ", " + \
					humanReadableBpsResults(deviceRec);
		}

		// Log total traffic values of network
		logMsg += "\n\tNetwork totals:\n\t\t" + humanReadableBpsResults(netTotalBps);

		logger.log(logMsg);

		// Store metrics to DB
		saveMetrics(TRAFFIC_LOAD_DEVICES_TABLE, deviceBps);
		saveMetric(TRAFFIC_LOAD_ENT_NET_TABLE, netTotalBps);
	}
}
