/*
 * WirelessSignal.h
 *
 *  Created on: 20 Feb 2019
 *      Author: sgasper
 */

#ifndef WIRELESSSIGNAL_H_
#define WIRELESSSIGNAL_H_

#include <iostream>
#include <thread>
#include "CollectorABC.h"
#include "debugnet.h"
using namespace std;

const string WIRELESS_SIGNAL_LOG_FILE = "/var/log/debugnet-collector-wifi-signal.log";


class WirelessSignal: public CollectorABC {
private:
	int waitPeriod;
	Logger logger;
	vector<string> allNics;

	string fetchWirelessStationDump();
	vector<MetricRecord> parseDump(string rawDumpOut, time_t execTime);

public:
	WirelessSignal();
	~WirelessSignal() {};
	string getCollectorName();
	string getLogFilePath();
	void measure();
};

#endif /* WIRELESSSIGNAL_H_ */
