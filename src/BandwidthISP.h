/*
 * Bandwidth.h
 *
 *  Created on: 23 Feb 2019
 *      Author: sgasper
 */

#ifndef BANDWIDTHISP_H_
#define BANDWIDTHISP_H_

#include <iostream>
#include <thread>

#include "CollectorABC.h"
#include "debugnet.h"

using namespace std;

const string BANDWIDTH_LOG_FILE = "/var/log/debugnet-collector-bandwidth-isp.log";


class BandwidthISP: public CollectorABC {
private:
	Logger logger;
	int waitPeriod;

	string testBandwidth();
	void parseToolOut(string testOut, int& up, int& down, double& latency);
public:
	BandwidthISP();
	string getCollectorName();
	string getLogFilePath();
	void measure();
};

#endif /* BANDWIDTHISP_H_ */
