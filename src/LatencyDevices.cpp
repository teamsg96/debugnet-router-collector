/*
 * Latency.cpp
 *
 *  Created on: 16 Jan 2019
 *      Author: sgasper
 */

#include "LatencyDevices.h"

LatencyDevices::LatencyDevices(){
	logger.setFile(LATENCY_LOG_FILE);
	waitPeriod = getConfig("latency_wait_between_tests").getValue<int>();
}

/*
 * Build the 'fping' command string.
 *
 * The fping command is what DebugNet uses to measure the latency of every
 * desired host.
 */
string LatencyDevices::buildFpingCmd(){
	string cmd;

	// Command name
	cmd += FPING_CMD_NAME;

	// Don't show per target/ping results
	cmd += " -q";

	// Number of times to ping each host
	cmd += " --vcount=" + to_string(PINGS_PER_HOST);

	// Add space before start of host list
	cmd += " ";

	// Add each host to ping
	for (Device host : hosts){
		cmd += host.getIpAddress() + " ";
	}

	return cmd;
}

/*
 * Checks the exit code of fping. Some codes indicate a fatal error has
 * occurred with fping that will make its output meaningless or impossible to
 * parse. Whereas some exit codes either indicate success or success with some
 * issues (but doesn't affect the output of fping). The function will display a
 * message if appropriate and it will also return if whether or not the exit
 * code was fatal.
 */
bool LatencyDevices::checkExitCode(CommandResult fpingResults){
	bool fatal;
	string msg = "";

	// Process the exit code of fping.
	switch (fpingResults.exitCode){
		case FPING_EXIT_CODE_IP_NOT_FOUND:
			msg = "one or more IP addresses where not found";
			fatal = false;
			break;
		case FPING_EXIT_CODE_SUCCESS:
			fatal = false;
			break;

		case FPING_EXIT_CODE_UNREACHABLE_HOSTS:
			msg = "one or more hosts where unreachable";
			fatal = false;
			break;

		case FPING_EXIT_CODE_INVALID_CMD_ARGS:
			msg = "invalid command line arguments: " + fpingResults.cmd;
			fatal = true;
			break;

		case FPING_EXIT_CODE_SYS_CALL_FAILURE:
			msg = "system call failure";
			fatal = true;
			break;

		case LINUX_EXIT_CODE_CMD_NOT_FOUND:
			msg = FPING_CMD_NAME + " command not found";
			fatal = true;
			break;

		default:
			msg = "unexpected error";
			fatal = true;
	}

	// Display a warning or error message if set
	if (msg != ""){
		if (fatal == true){
			logger.log(msg, LOG_ERROR);
		}
		else{
			logger.log(msg, LOG_WARNING);
		}
	}

	return fatal;
}

/*
 * Return the name of this collector
 */
string LatencyDevices::getCollectorName(){
	return "Latency";
}

/*
 * Return the file path this collector logs to
 */
string LatencyDevices::getLogFilePath(){
	return LATENCY_LOG_FILE;
}

/*
 * Measure latency on the network and to the ISP
 */
void LatencyDevices::measure(){
	bool fatalExitCode;
	CommandResult fpingResults;
	string cmd;
	time_t timeOfExec;

	// Don't stop measuring until stopped externally
	while (true){
		logger.log("measuring network latency...");
		hosts.clear();

		// Fetch all online hosts from DB
		hosts = getAllOnlineDevices();

		cmd = buildFpingCmd();

		// Record the time the test was made
		timeOfExec = time(0);

		fpingResults = executeCmdOnRouter(cmd);

		fatalExitCode = checkExitCode(fpingResults);

		// Store results if no fatal errors occurred with fping.
		if (fatalExitCode == false){
			storeResults(fpingResults, timeOfExec);
		}
		else{
			throw CMDExecutionOnRouterException(cmd, fpingResults.out);
		}

		logger.log("waiting till next scan...");
		// Wait a set amount of time before measuring again
		this_thread::sleep_for(chrono::seconds(waitPeriod));
	}
}

/*
 * Parses a line of fping string output in order to produce a struct that holds
 * all the data on the line plus some additional statistics.
 *
 * Each line of output is of the following format:
 * <hostname>		: <pingTest1Latency>  <pingTest2Latency> <pingTestNLatency>
 */
void LatencyDevices::parseFpingLine(string rawLine, string& device, double& maxLatency,
					double& avgLatency, double& minLatency, double& packetLoss){
	double allLatencies[PINGS_PER_HOST];
	double totalLatency = 0;
	int pingsFailed = 0;

	// Split line by one or more spaces.
	regex pattern("\\s+");
	vector<string> lineParams{sregex_token_iterator(rawLine.begin(),
													rawLine.end(),
													pattern,
													-1), {}};

	// Set the host that the latency measurements belong to.
	device = lineParams[0];

	// Value '-1' indicates no value set.
	maxLatency = -1;
	avgLatency = -1;
	minLatency = -1;

	// Convert the latency value from every ping test into a double from
	// a string. If a ping was unsuccessful then the cell is left empty.
	for (int i = 0; i < PINGS_PER_HOST; i++){
		// The '-' character instead of a real number indicates a ping
		// was unsuccessful.
		if(lineParams[i+2] != "-"){
			// Store latency value and check/update min and max latency value
			// for host
			allLatencies[i] = atof(lineParams[i+2].c_str());
			totalLatency += allLatencies[i];

			if (maxLatency == -1 || allLatencies[i] > maxLatency){
				maxLatency = allLatencies[i];
			}

			if (minLatency == -1 || allLatencies[i] < minLatency){
				minLatency = allLatencies[i];
			}

		}
		else{
			// The symbol '-' indicates the ping test failed
			pingsFailed++;

			allLatencies[i] = -1;
		}
	}

	packetLoss = ((double) pingsFailed / (double) PINGS_PER_HOST) * 100;

	// Only calculate the average if full packet loss hasn't occurred.
	if (packetLoss != FULL_PACKET_LOSS){
		avgLatency = (int)((totalLatency / (PINGS_PER_HOST - pingsFailed))
				* 100.0) / 100.0;
	}

	return;
}

/*
 * Takes the output of fping and stores it in the data base.
 */
void LatencyDevices::storeResults(CommandResult fpingResults, time_t timeOfExec){
	istringstream outStream(fpingResults.out);
	string rawLine;
	stringstream logMsg;
	vector<MetricRecord> deviceMetrics;

	logMsg << "latency results...";

	// Each string line needs to be parsed in order to extract the data it
	// holds. Each line contains a host name and its latency measurement from
	// each ping test.
	while(getline(outStream, rawLine)){
		string hostAddress;
		double maxLatency;
		double avgLatency;
		double minLatency;
		double packetLoss;

		// Parse the output line so that each of its elements can be interacted
		// with.
		parseFpingLine(rawLine, hostAddress, maxLatency, avgLatency, minLatency,
				packetLoss);

		// Log latency measurements for host. Only include min/avg/max if
		// at least one ping was successful, this simplifies the output.
		logMsg << "\n\tHost: " << hostAddress << ", ";
		if (packetLoss != FULL_PACKET_LOSS){
			logMsg << "min/avg/max = " << \
					fixed << setprecision(2) << minLatency << "/" << \
					fixed << setprecision(2) << avgLatency << "/" << \
					fixed << setprecision(2) << maxLatency << ", ";
		}

		logMsg << "packet loss = " << fixed << setprecision(2) << packetLoss;

		// Generate a metric record to hold latency readings for the host
		MetricRecord record(timeOfExec);

		// Ignore max, avg and min if no latency readings where made
		if (avgLatency != -1){
			record.addParam("max_latency", maxLatency);
			record.addParam("avg_latency", avgLatency);
			record.addParam("min_latency", minLatency);
		}

		record.addParam("loss", packetLoss);

		// Metric entries into the DB requires the MAC address associated
		// to the device
		string hostMac;
		for (Device host : hosts){
			if (host.getIpAddress() == hostAddress){
				hostMac = host.getMacAddress();
			}
		}

		record.addParam("device", hostMac);
		deviceMetrics.push_back(record);
	}

	logger.log(logMsg.str());

	// Store latency measurements for every networked host in the database and
	// for the ISP node.
	saveMetrics(LATENCY_DEVICE_TABLE, deviceMetrics);
}
