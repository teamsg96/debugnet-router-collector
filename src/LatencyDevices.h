/*
 * Latency.h
 *
 *  Created on: 16 Jan 2019
 *      Author: sgasper
 */

#ifndef APPS_COLLECTOR_LATENCY_H_
#define APPS_COLLECTOR_LATENCY_H_

#include <chrono>
#include <iomanip>
#include <iostream>
#include <thread>
#include <regex>
#include <sstream>
#include <vector>

#include "CollectorABC.h"
#include "debugnet.h"

using namespace std;

const string FPING_CMD_NAME = "fping";					// Fping command name
const short int FULL_PACKET_LOSS = 100;					// Percent that equals full packet loss
const string LATENCY_LOG_FILE = "/var/log/debugnet-collector-latency.log";

// Fping exit codes
const short int FPING_EXIT_CODE_SUCCESS = 0;
const short int FPING_EXIT_CODE_UNREACHABLE_HOSTS = 1;
const short int FPING_EXIT_CODE_IP_NOT_FOUND = 2;
const short int FPING_EXIT_CODE_INVALID_CMD_ARGS = 3;
const short int FPING_EXIT_CODE_SYS_CALL_FAILURE = 4;

// Latency measuring cfg
const short int PINGS_PER_HOST = 3;						// Number of times each host is pinged


class LatencyDevices: public CollectorABC {
private:
	Logger logger;
	vector <Device> hosts;
	int waitPeriod;

	string buildFpingCmd();
	bool checkExitCode(CommandResult fpingResults);
	void parseFpingLine(string rawLine, string& host, double& maxLatency,
			double& avgLatency, double& minLatency, double& packetLoss);
	void storeResults(CommandResult fpingResults, time_t timeOfExec);
public:
	LatencyDevices();
	string getCollectorName();
	string getLogFilePath();
	void measure();
};



#endif /* APPS_COLLECTOR_LATENCY_H_ */
