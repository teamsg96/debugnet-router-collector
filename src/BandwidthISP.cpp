/*
 * Bandwidth.cpp
 *
 *  Created on: 23 Feb 2019
 *      Author: sgasper
 */

#include "BandwidthISP.h"

BandwidthISP::BandwidthISP() {
	logger.setFile(BANDWIDTH_LOG_FILE);
	waitPeriod = getConfig("bandwidth_wait_between_tests").getValue<int>();
}

/*
 * Test bandwidth to ISP
 */
string BandwidthISP::testBandwidth() {
	string cmd = "speedtest-cli --csv --no-pre-allocate";

	CommandResult result = executeCmdOnRouter(cmd);

	if (result.exitCode != 0){
		// The command may fail if their is no internet connection. If this is
		// the case then display a warning message but don't raise an exception.
		if (result.out.find("<urlopen error [Errno -3] Try again>") != string::npos){
			logger.log("no Internet connection ... skipping test", LOG_WARNING);
			return "";
		}
		// The command may fail if their is not enough memory to execute the
		// test.
		else if (result.out.find("MemoryError") != string::npos ||
				 result.out.find("Insufficient memory") != string::npos){
			logger.log("insufficient memory to run test ... skipping test", LOG_WARNING);
			return "";
		}
		// This message may occur when a connection is to slow to execute the
		// test.
		else if (result.out.find("read operation timed out") != string::npos){
			logger.log("taking to long to execute test ... skipping test", LOG_WARNING);
			return "";
		}
		else{
			logger.log(result.out);
			logger.log("speedtest-cli failed to run ... skipping test\n", LOG_WARNING);
			return "";
		}
	}
	else{
		return result.out;
	}
}

/*
 * Parse string output from 'speedtest-cli'
 */
void BandwidthISP::parseToolOut(string testOut, int& up, int& down, double& latency) {
	// The speedtest tool produces its output in CSV form. In order to know the
	// field name of each 'cell' its output we use the tool fetch all field
	// names.
	string headerCmd = "speedtest-cli --csv-header";
	CommandResult headerResult = executeCmdOnRouter(headerCmd);
	if (headerResult.exitCode != 0){
		throw CMDExecutionOnRouterException(headerResult.cmd, headerResult.out);
	}

	// Break up header output and the test output
	vector<string> headerCsv = parseCsvLine(headerResult.out);
	vector<string> outputCsv = parseCsvLine(testOut);

	// Default all the parameters we care about to 'not found'
	up = -1;
	down = -1;
	latency = -1;

	// Locate the field names we are interested in. Use the position of the field
	// to extract the data from the test output.
	for (unsigned int i=0; i<headerCsv.size(); i++){
		if (headerCsv[i] == "Download"){
			down = stoi(outputCsv[i]);
		}
		else if (headerCsv[i] == "Upload"){
			up = stoi(outputCsv[i]);
		}
		else if (headerCsv[i] == "Ping"){
			latency = stod(outputCsv[i]);
		}
	}

	if (up == -1 || down == -1 || latency == -1){
		DebugnetException("unable to extract all required parameters from "\
				"speedtest-cli output");
	}
}

string BandwidthISP::getCollectorName() {
	return "BandwidthISP";
}

string BandwidthISP::getLogFilePath() {
	return BANDWIDTH_LOG_FILE;
}

// Continuously measure ISP bandwidth, with a pause between each measure
void BandwidthISP::measure() {
	while(true){
		logger.log("waiting till next test...");
		// Wait a set amount of time before measuring again
		this_thread::sleep_for(chrono::seconds(waitPeriod));

		logger.log("testing ISP bandwidth...");

		// Execute bandwidth test
		string testOut = testBandwidth();

		// Don't bother parsing output if no output was obtained. Try again on
		// next loop.
		if (testOut == ""){
			continue;
		}

		// Parse test tool output
		int up;
		int down;
		double latency;
		parseToolOut(testOut, up, down, latency);

		time_t execTime = time(0);

		// Prepare bandwidth record for DB
		MetricRecord bandwidthRecord(execTime);
		bandwidthRecord.addParam("up", up);
		bandwidthRecord.addParam("down", down);

		// Prepare latency record for DB
		MetricRecord latencyIspRecord(execTime);
		latencyIspRecord.addParam("latency", latency);

		// Store metrics to DB
		saveMetric(BANDWIDTH_ISP_TABLE, bandwidthRecord);
		saveMetric(LATENCY_ISP_TABLE, latencyIspRecord);

		stringstream logMsg;
		logMsg << "Download: " << down << ", Upload: " << up << ", Latency: " << latency;
		logger.log(logMsg.str());
	}
}
