/*
 * CollectorABC.h
 *
 *  Created on: 16 Jan 2019
 *      Author: sgasper
 */

#ifndef APPS_COLLECTOR_COLLECTORABC_H_
#define APPS_COLLECTOR_COLLECTORABC_H_

#include <iostream>
using namespace std;

class CollectorABC {
public:
	virtual ~CollectorABC() {};
	virtual string getCollectorName() = 0;
	virtual string getLogFilePath() = 0;
	virtual void measure() = 0;
};



#endif /* APPS_COLLECTOR_COLLECTORABC_H_ */
